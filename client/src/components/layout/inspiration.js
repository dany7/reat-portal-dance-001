import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import placeholder from '../images/Placeholder-1.png';

class InspirationPage extends Component {
    render() {
        return (
            <Container>
                <Row>
                    <h1>Videos and Inspiration</h1>
                        <p>ed ut perspiciatis unde omnis iste natus error sit 
                        voluptatem accusantium doloremque laudantium, totam rem aperiam, 
                        eaque ipsa quae ab illo inventore veritatis et quasi architecto 
                        beatae vitae</p>
                </Row>
                <Row>
                    <Col>
                        <h3>Hip Hop Fundamentals</h3>
                            <p>ed ut perspiciatis unde omnis iste natus error sit 
                            voluptatem accusantium doloremque laudantium, totam rem aperiam, 
                            eaque ipsa quae ab illo inventore veritatis et quasi architecto 
                            beatae vitae</p>
                    </Col>
                    <Col> 
                        <img src={placeholder} alt="" />
                        <img src={placeholder} alt="" />
                        <img src={placeholder} alt="" />
                        <img src={placeholder} alt="" />
                        <img src={placeholder} alt="" />
                        <img src={placeholder} alt="" />
                        <p>eaque ipsa quae ab illo inventore veritatis et</p>
                    </Col>
                </Row>
                <Row>
                    <h2>Outfits</h2>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, 
                        sed diam nonummy nibh euismod tincidunt ut laoreet dolore 
                        magna aliquam erat volutpat. Ut wisi enim ad minim veniam, 
                        quis nostrud exerci tation ullamcorper suscipit lobortis nisl 
                        ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor
                        in hendrerit in vulputate velit esse molestie consequat, </p>
                </Row>
                <Row>
                    <img src={placeholder} alt="" />
                    <img src={placeholder} alt="" />
                    <img src={placeholder} alt="" />
                    <img src={placeholder} alt="" />
                    <img src={placeholder} alt="" />
                </Row>
                <Row>
                    <h3>External Resources</h3>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
                    nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi 
                    enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis 
                    nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit 
                    in vulputate velit esse molestie consequat </p>
                </Row>
                <Row>
                    <ul>
                    <li>
                    <h4>External resource #1</h4>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,</p>
                    </li>
                    <li>
                    <h4>External resource #2</h4>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,</p>
                    </li>
                    <li>
                    <h4>External resource #3</h4>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,</p>
                    </li>
                    </ul>  
                </Row>
            </Container>
        );
    }
}

export default InspirationPage;