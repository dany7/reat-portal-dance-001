const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const userSchema = new Schema ({ 
    username: {type: String, required: true, unique: true, trim: true, minlength: 5},
    email: {type: String,vrequired: true},
    name: {type: String, required: true},
    password: {type: String, required: true, minlength: 8 }
}, 
{timestamps: true}
);

const User = mongoose.model('users', userSchema);
module.exports = User;