//Middlewares and dependencies 
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const dotenv = require("dotenv");
const passport = require("passport");
const usersRouter = require('./routes/users');

dotenv.config();


//Express server
const app = express();
const port = process.env.PORT || 5001;


//starting the server 
app.use(cors());
app.use(express.urlencoded({extended: true}));
app.use(express.json());

//database connection
const uri = process.env.ATLAS_URI
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true })
  .then(() => {
    console.log('Connected to the database successfully! Welcome, Mr Specter...')
  })
  .catch(err => console.log(err))

// Passport middleware
app.use(passport.initialize());

// Passport config
require("./config/passport")(passport);

//models declaration on server 
app.use('/users', usersRouter);


app.listen( port, () => {
    console.log(`Server is running on port: ${port}`);
} )

